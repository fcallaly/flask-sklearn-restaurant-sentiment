from flask import Flask, request
from flask_restplus import Resource, Api, fields
import joblib

import prepare_review

app = Flask(__name__)
api = Api(app)

ns = api.namespace('reviews', description='Review operations')

review = api.model('Review', {
    'review': fields.String(required=True, description='The review text')
    })

classifier = joblib.load('restaurant_review_classifier.joblib')
cv = joblib.load('restaurant_review_countvectorizer.joblib')

@ns.route('/')
class Review(Resource):
    def get(self):
        return {'test': 'reviews'}

    @ns.expect(review)
    def post(self):
        prepared_review = prepare_review.prepare_review(api.payload['review'])
        feature_set = cv.transform([prepared_review]).toarray()
        prediction = int(classifier.predict(feature_set)[0])

        print('Sentiment prediction is: ', prediction)

        return {'review': api.payload['review'],
                'sentiment': prediction}

if __name__ == '__main__':
    app.run(debug=True)
