import re
import nltk
nltk.download('stopwords')

from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer

corpus = []
stopwordsSet = set(stopwords.words('english'))
    
ps = PorterStemmer()

def prepare_review(review_text):
    review = re.sub('[^a-zA-Z]', ' ', review_text)
    review = review.lower()
    review = review.split()
    
    review_words = []
    for word in review:
        if not word in stopwordsSet:
            review_words.append(word)
    
    #review = [ ps.stem(word) for word in review if not word in stopwordsSet ]
    
    review = ' '.join(review_words)
    
    return review

